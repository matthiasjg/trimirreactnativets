# TrimirReactNativeTS

An attempt at building a life journal app for you and your tribe peers — No Internet Required.

## Status

![](./screenshots/Screenshot_1576513769_nodejs-mobile.png)![](./screenshots/Screenshot_1576513775_nodejs-mobile_alert.png)![](./screenshots/Screenshot_1576575447_express_alert.png)![](./screenshots/Screenshot_1576575573_express_chrome.png)![](./screenshots/Screenshot_1578588031_sitejs_alert.png)![](./screenshots/Screenshot_1578587924_sitejs_chrome_local_cert.png)

Early WIP, a.k.a **Just getting started.**

## Building Blocks

- [React Native](https://facebook.github.io/react-native/d) and [TypeScript](https://www.typescriptlang.org/)
- [Site.js](https://sitejs.org/) ~~eventually~~...
- ...

## Development Environment

Linux, Ubuntu.

### Install and configure Android Studio

<https://developer.android.com/studio/>

```bash
# ~/.bashrc
#export ANDROID_HOME=$HOME/Android/Sdk
#export ANDROID_SDK_ROOT=$ANDROID_HOME
#export PATH=$PATH:$ANDROID_HOME/emulator
#export PATH=$PATH:$ANDROID_HOME/tools
#export PATH=$PATH:$ANDROID_HOME/tools/bin
#export PATH=$PATH:$ANDROID_HOME/platform-tools
```

### Install and configure adoptopenjdk-8-hotspot-amd64

<https://adoptopenjdk.net/installation.html#linux-pkg>

```bash
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
sudo apt-get install -y software-properties-common
sudo apt-get install adoptopenjdk-8-hotspot
sudo update-java-alternatives --set adoptopenjdk-8-hotspot-amd64
# ~/.bashrc
#export JAVA_HOME=/usr/lib/jvm/adoptopenjdk-8-hotspot-amd64/
```

### Install and configure Android NDK and CMake

<https://developer.android.com/studio/projects/install-ndk>

> The Android NDK is a toolset that lets you implement parts of your app in native code, using languages such as C and C++.

```bash
# ~/.bashrc
#export ANDROID_NDK_HOME=$ANDROID_SDK_ROOT/ndk/20.1.5948944
```

### Install React Native

<https://facebook.github.io/react-native/>

Install [Expo](https://expo.io/):

```bash
#sudo apt install gradle
sudo npm install -g expo-cli
```

### Build and Install Watchman

<https://facebook.github.io/watchman/>

```bash
sudo apt-get install -y autoconf automake build-essential python-dev libtool pkg-config libssl-dev
cd ~/Code
git clone https://github.com/facebook/watchman.git -b v4.9.0 --depth 1
cd watchman
./autogen.sh
#./configure
# watchman mercurial error
./configure --without-python --without-pcre --enable-lenient
make
sudo make install
```

### Add Node.js for Mobile Apps

<https://code.janeasystems.com/nodejs-mobile>

```bash
#https://code.janeasystems.com/nodejs-mobile/getting-started-react-native
# ./.bashrc
#export ANDROID_NDK_HOME=$ANDROID_SDK_ROOT/ndk/20.1.5948944
npm install nodejs-mobile-react-native --save
#npx react-native link nodejs-mobile-react-native
```

## Troubleshooting

### Unable to load script from assets 'index.android.bundle'...

```bash
mkdir -p android/app/src/main/assets
cd android/ && ./gradlew clean && -
npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/rescd
```

### java.lang.RuntimeException: Node assets copy failed

Stack trace: `Caused by: java.io.FileNotFoundException: nodejs-project/node_modules/resolve/test/resolver/symlinked/_/node_modules/foo.js`

Work around: inside `android {` / `defaultConfig {` in two gradle files inside your react-native project folder `android/app/build.gradle` and `node_modules/nodejs-mobile-react-native/android/build.gradle`:

```
...
android {
    ...
    defaultConfig {
        ...
        aaptOptions {
            ignoreAssetsPattern '!.svn:!.git:!.ds_store:!*.scc:!CVS:!thumbs.db:!picasa.ini:!*~'
        }
    }
...
}
```

<https://github.com/JaneaSystems/nodejs-mobile/issues/60#issuecomment-378088756>

### Error: EACCES: permission denied, mkdir '/data/.small-tech.org'

On Run Console of Android Studio:

```bash
I/NODEJS-MOBILE:  💖 Site.js v12.10.4 (running on Node v10.13.0)
E/NODEJS-MOBILE: /data/data/com.trimirreactnativets/files/nodejs-project/node_modules/fs-extra/lib/mkdirs/mkdirs-sync.js:45
            throw err0
            ^

    Error: EACCES: permission denied, mkdir '/data/.small-tech.org'
        at Object.mkdirSync (fs.js:750:3)
        at mkdirsSync (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/fs-extra/lib/mkdirs/mkdirs-sync.js:31:9)
        at Object.mkdirsSync (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/fs-extra/lib/mkdirs/mkdirs-sync.js:36:14)
        at new Site (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-tech/site.js/index.js:100:8)
        at require.weCanBindToPort (/data/data/com.trimirreactnativets/files/nodejs-project/main.js:21:18)
        at Ensure.weCanBindToPort (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-tech/site.js/bin/lib/ensure.js:165:7)
        at Object.<anonymous> (/data/data/com.trimirreactnativets/files/nodejs-project/main.js:12:47)
        at Module._compile (internal/modules/cjs/loader
    .js:688:30)
        at Object.Module._extensions..js (internal/modules/cjs/loader.js:699:10)
        at Module.load (internal/modules/cjs/loader.js:598:32)
```

```javascript
/* Copyright (C) 2018-2019 The Manyverse Authors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// https://gitlab.com/staltz/manyverse/blob/master/src/backend/loader.ts
// Set default directory
const nodejsProjectDir = path.resolve(rnBridge.app.datadir(), 'nodejs-project')
os.homedir = () => nodejsProjectDir
process.cwd = () => nodejsProjectDir
```

### INSTALL_FAILED_INSUFFICIENT_STORAGE

```bash
Execution failed for task ':app:installDebug'.
> com.android.builder.testing.api.DeviceException: com.android.ddmlib.InstallException: INSTALL_FAILED_INSUFFICIENT_STORAGE
```

Solution for working w/ emulator: increase size of internal storage in Android Virtual Device Advanced Settings (e.g. from default `800MB` to `900MB`).

Force to install the app to the external storage, by adding to the application's manifest file: `android:installLocation="preferExternal"`.

```xml
<manifest… android:installLocation="preferExternal"/>
```

### Error: Unsupported platform

```bash
I/NODEJS-MOBILE:  💖 Site.js v12.10.4 (running on Node v10.13.0)
I/NODEJS-MOBILE:  🚧 [Site.js] Using locally-trusted certificates.
I/NODEJS-MOBILE:  🔒 [@small-tech/https] Creating server at localhost with locally-trusted certificates.
I/NODEJS-MOBILE:  🆕 [Nodecert] Setting up…
E/NODEJS-MOBILE: /data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@ind.ie/nodecert/index.js:108
      if (platform === undefined) throw new Error('Unsupported platform', _platform)
                                  ^

    Error: Unsupported platform
        at mkcertBinaryForThisMachine (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@ind.ie/nodecert/index.js:108:37)
        at module.exports (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@ind.ie/nodecert/index.js:28:26)
        at Object.Greenlock.createServer (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-tech/https/index.js:162:7)
        at Site.createServer (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-tech/site.js/index.js:399:18)
        at Site.endAppConfigurationAndCreateServer (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-tech/site.js/index.js:244:24)
        at new Site (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-
E/NODEJS-MOBILE: tech/site.js/index.js:137:10)
        at require.weCanBindToPort (/data/data/com.trimirreactnativets/files/nodejs-project/main.js:32:18)
        at Ensure.weCanBindToPort (/data/data/com.trimirreactnativets/files/nodejs-project/node_modules/@small-tech/site.js/bin/lib/ensure.js:165:7)
        at Object.<anonymous> (/data/data/com.trimirreactnativets/files/nodejs-project/main.js:23:47)
        at Module._compile (internal/modules/cjs/loader.js:688:30)
```

Work around: in `nodejs-assets/nodejs-project/node_modules/@ind.ie/nodecert/index.js`:

```javascript
  const platformMap = {
    linux: 'linux',
    darwin: 'darwin',
    win32: 'windows',
    android: 'linux' // ‹— add
  }

  const architectureMap = {
    arm: 'arm',
    x64: 'amd64',
    ia32: 'amd64' // ‹— add
  }

// [...]

function tryToInstallTheDependency() {
  if (_platform === 'android') { // ‹— add
    // do nothing, let's use the pre-build binary shipped w/ nodecert
  } else if (_platform === 'linux') {…
```

### Error: could not start HTTP server for @small-tech/https.

```bash
I/NODEJS-MOBILE:  💖 Site.js v12.10.4 (running on Node v10.13.0)
I/NODEJS-MOBILE:  🚧 [Site.js] Using locally-trusted certificates.
I/NODEJS-MOBILE:  🔒 [@small-tech/https] Creating server at localhost with locally-trusted certificates.
     🆕 [Nodecert] Setting up…
I/NODEJS-MOBILE:  🖊  [Nodecert] Creating local certificate authority (local CA) using mkcert…
I/NODEJS-MOBILE:  🎉 [Nodecert] Local certificate authority created.
     📜 [Nodecert] Creating local TLS certificates using mkcert…
I/NODEJS-MOBILE:  🎉 [Nodecert] Local TLS certificates created.
I/NODEJS-MOBILE:  🔒 [@small-tech/https] Created HTTPS server.
I/NODEJS-MOBILE:  🌍 [Site.js] Using globally-trusted certificates.
     👉 [Site.js] Aliases: also responding for www.localhost.
I/NODEJS-MOBILE:  🔒 [@small-tech/https] Creating server with globally-trusted Let’s Encrypt certificates.
    Will look for certs in /data/user/0/com.trimirreactnativets/files/nodejs-project/.small-tech.org/site.js/tls/global
I/NODEJS-MOBILE:  🔒 [@small-tech/https] Using Let’s Encrypt production server.
I/NODEJS-MOBILE:  🔒 [@small-tech/https] Created HTTPS server.
I/NODEJS-MOBILE:  🤯 Error: could not start HTTP server for @small-tech/https.
```

The SSL certs are NOT being created. Unfort. the pre-build `mkcert-*` binaries (e.g. `nodejs-project/node_modules/@ind.ie/nodecert/mkcert-bin/mkcert-v1.4.0-linux-amd64`, in `/data/user/0/com.trimirreactnativets/files`)
) are not executable (missing the `x` flag).

Work around: `chmod +x nodejs-assets/nodejs-project/node_modules/@ind.ie/nodecert/mkcert-bin/*`

## Android Dev Tips

```bash
adb shell
generic_x86:/ $
# su
# run-as
```

## License

[AGPLv3 or later](./LICENSE)
