const rnBridge = require('rn-bridge')

// https://source.ind.ie/site.js/app#api
const Site = require('@small-tech/site.js')
const express = require('express')
const path = require('path')
const os = require('os')

const app = express()
const port = 3443

/* Copyright (C) 2018-2019 The Manyverse Authors.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// https://gitlab.com/staltz/manyverse/blob/master/src/backend/loader.ts
// Set default directory
const nodejsProjectDir = path.resolve(rnBridge.app.datadir(), 'nodejs-project')
console.log('nodejsProjectDir', nodejsProjectDir)
os.homedir = () => nodejsProjectDir
process.cwd = () => nodejsProjectDir

// console.log('os.platform:', os.platform) // android
// console.log('os.arch:', os.arch) // ia32

require('@small-tech/site.js/bin/lib/ensure').weCanBindToPort(port, () => {
  // One can safely bind to port on Linux now, likely even a ‘privileged’ one
  app.use(express.static(path.join(__dirname, 'public')))
  // app.listen(port, () => rn_bridge.channel.send(` 🎉 Express serving on port ${port}`));

  const options = {
    global: false //  using globally-trusted Let’s Encrypt TLS certificates instead, set this to {global: true}
    // , certificateDirectory: path.join(os.homedir(), '.trimir', 'site.js', 'tls') // Specify custom certificate directory for Site.js.
  }
  const server = new Site().createServer(options, app).listen(port, () => { // eslint-disable-line no-unused-vars
    rnBridge.channel.send(` 🎉 Site.js Serving on ${port}`)
  })
})

// Echo every message received from react-native.
rnBridge.channel.on('message', (msg) => {
  rnBridge.channel.send(msg)
})

// Inform react-native node is initialized.
// rnBridge.channel.send("Node was initialized.");
