/**
 * @format
 */

import React from 'react'
import { AppRegistry } from 'react-native'
import App from './App'
import { name as appName } from './app.json'
import nodejs from 'nodejs-mobile-react-native'

class EntryPoint extends React.Component {
  UNSAFE_componentWillMount () { // eslint-disable-line camelcase
    nodejs.start('main.js')
    nodejs.channel.addListener(
      'message',
      msg => {
        alert('From node: ' + msg) // eslint-disable-line no-undef
      },
      this
    )
  }

  render () {
    return <App />
  }
}

AppRegistry.registerComponent(appName, () => EntryPoint)
